First Features
==============

This section describes the 'First Features' that are computed merely by collecting data of previous games.

There are roughly six groups of features:

- General Match Information:
    These features contain the most common meta data of the matches.

- Current Season Statistics ("Cur"):
    These features contain statistics of the playing teams from the current season.

- Previous Season Statistics ("Fin"):
    These features contain the playing teams' statistics after the final match day of the previous season.

- Recent Strength in General ("RSIG"):
    These features contain information about the playing teams' shape during their last 2 * n matches, where n is a natural number.
    More precisely, n is stored in the python variable 'minimum_matches_before' and means
    that for each of the two teams, 2 * n matches are considered.
    The '2' results from the idea, that home games as well as away games are considered for each team
    contrary to the RSBL-idea below,
    where only home games are considered for the home team and only away matches are considered for the away team.

- Recent Strength by Location ("RSBL"):
    These features contain information about the playing teams' shape during their last n matches with the same state (home state or away state),
    where n is a natural number.
    More precisely, n is stored in the python variable 'minimum_matches_before' and means
    that for the home team, the last n home games are considered,
    and  for the away team, the last n away games are considered.

- Direct Comparison ("DC"):
    These features describe numbers of goals during up to three (if available) last matches where both teams met.

Here, each feature is described by asking the question to which it is the answer (HT means 'Home Team' and AT means 'Away Team'):

- Season Start Year
    What is the year in which the season this match belongs to started?
- Match Day
    What is the match day of the season? (There are 34 match days in the German Bundesliga.)
- Home Team
    What is the name of the home team?
- Away Team
    What is the name of the away team?

- Cur HT Rank
    What is the current rank of the home team?
- Cur AT Rank
    What is the current rank of the away team?
- Cur HT Points
    What is the current number of points of the home team? (In the German Bundesliga, a win is worth 3 points, a draw 1, and a loss 0.)
- Cur AT Points
    What is the current number of points of the away team?
- Cur HT Goals Shot
    How many goals has the home team already scored during the current season?
- Cur AT Goals Shot
    How many goals has the away team already scored during the current season?
- Cur HT Goals Conceded
    How many goals has the home team already conceded during the current season?
- Cur AT Goals Conceded
    How many goals has the away team already conceded during the current season?

- Participated Last Season HT
    Has the home team participated in this league during the previous season?
- Participated Last Season AT
    Has the away team participated in this league during the previous season?
- Fin HT Rank
    What was the rank of the home team after the final match day of the previous season?
- Fin AT Rank
    What was the rank of the away team after the final match day of the previous season?
- Fin HT Points
    How many points did the home team earn during the previous season?
- Fin AT Points
    How many points did the away team earn during the previous season?
- Fin HT Goals Shot
    How many goals did the home team shoot during the previous season?
- Fin AT Goals Shot
    How many goals did the away team shoot during the previous season?
- Fin HT Goals Conceded
    How many goals did the home team concede during the previous season?
- Fin AT Goals Conceded
    How many goals did the away team concede during the previous season?

- RSIG Matches Before HT
    How many matches of the home team are in the data prior to this match?
- RSIG Matches Before AT
    How many matches of the away team are in the data prior to this match?
- RSIG HT Points
    How many points did the home team earn during its last 2 * n matches?
- RSIG AT Points
    How many points did the away team earn during its last 2 * n matches?
- RSIG HT Goals Shot
    How many goals did the home team score during its last 2 * n matches?
- RSIG AT Goals Shot
    How many goals did the away team score during its last 2 * n matches?
- RSIG HT Goals Conceded
    How many goals did the home team concede during its last 2 * n matches?
- RSIG AT Goals Conceded
    How many goals did the away team concede during its last 2 * n matches?
- RSBL HT Points
    How many points did the home team earn during its last n home games?
- RSBL AT Points
    How many points did the away team earn during its last n away games?
- RSBL HT Goals Shot
    How many goals did the home team score during its last n home games?
- RSBL AT Goals Shot
    How many goals did the away team score during its last n away games?
- RSBL HT Goals Conceded
    How many goals did the home team concede during its last n home games?
- RSBL AT Goals Conceded
    How many goals did the away team concede during its last n away games?

- DC Have Met 1
    Have the two teams met at least once prior to this game?
- DC 1 Goals Shot HT
    How many goals did the home team of this match score in the last match where both teams met prior to this game?
    (The home team of this match can have been either the home team or the away team of that game in the past. It does not matter.)
- DC 1 Goals Shot AT
    How many goals did the away team of this match score in the last match where both teams met prior to this game?
    (The away team of this match can have been either the home team or the away team of that game in the past. It does not matter.)
- DC Have Met 2
    Have the two teams met at least twice prior to this game?
- DC 2 Goals Shot HT
    How many goals did the home team of this match score in the last but one match where both teams met prior to this game?
    (The home team of this match can have been either the home team or the away team of that game in the past. It does not matter.)
- DC 2 Goals Shot AT
    How many goals did the away team of this match score in the last but one match where both teams met prior to this game?
    (The away team of this match can have been either the home team or the away team of that game in the past. It does not matter.)
- DC Have Met 3
    Have the two teams met at least thrice prior to this game?
- DC 3 Goals Shot HT
    How many goals did the home team of this match score in the last but two match where both teams met prior to this game?
    (The home team of this match can have been either the home team or the away team of that game in the past. It does not matter.)
- DC 3 Goals Shot AT
    How many goals did the away team of this match score in the last but two match where both teams met prior to this game?
    (The away team of this match can have been either the home team or the away team of that game in the past. It does not matter.)


    
Additional Features
===================

This section describes the 'Additional Features' that are computed exclusively from the 'First Features'.

Note: For all real numbers x and all even natural numbers p, we have x^p = (-x)^p.
      Since losing the sign of x is not wanted here, x * abs(x)^(p - 1) is used instead of x^p.
      
Diff means 'difference' and Pow means 'power'.

For each integer p in {1, 2, 3, 4, 5}, the following features are defined:

- Cur Points Diff Pow p
    "Cur Points Diff Pow p"          = ("Cur HT Points" - "Cur AT Points") * abs("Cur HT Points" - "Cur AT Points")^(p - 1)
- Fin Points Diff Pow p
    "Fin Points Diff Pow p"          = ("Fin HT Points" - "Fin AT Points") * abs("Fin HT Points" - "Fin AT Points")^(p - 1)
- Cur Points Diff Pow p Weighted
    "Cur Points Diff Pow p Weighted" =       "Match Day"  * "Cur Points Diff Pow p"
- Fin Points Diff Pow p Weighted
    "Fin Points Diff Pow p Weighted" = (34 - "Match Day") * "Fin Points Diff Pow p"
    (34 is the maximum "Match Day" in the German Bundesliga, so this value must be adapted for other leagues.)
- Weighted Points Diff Pow p Sum
    "Weighted Points Diff Pow p Sum" = "Cur Points Diff Pow p Weighted" + "Fin Points Diff Pow p Weighted"
