Project Goals
-------------

In this project, I'm developing machine learning models to predict the general outcome of sports matches.
More precisely, I want to classify whether the final result of a match is going to be
a home team win, an away team win, or a draw by analysing lists containing results of previous matches.

These lists are of the following simple form:

|                | Match Day | Home Team      | Result | Away Team      |
|----------------|-----------|----------------|--------|----------------|
| Example Game 1 | 34        | Frankfurt      | 2 : 1  | Hamburg        |
| Example Game 2 | 34        | Berlin         | 1 : 3  | München        |
| Example Game 3 | 34        | Stuttgart      | 2 : 2  | Dresden        |

Additionaly, a value for the starting year of the season is provided.
The example above is a football league, but other games should work as well.



Data
----

The raw data to test the models comprises 30 seasons of the German Bundesliga and is collected from public websites like
[https://www.football-data.co.uk/](https://www.football-data.co.uk/) or [https://www.fussballdaten.de](https://www.fussballdaten.de).
It is stored in the "raw_data" subfolder.



First Features from Previous Games
----------------------------------

Note that the four columns shown above are the only sources to train the models. It is possible though
to collect information from all the lines of previous match days and seasons to compute features
that can be used for machine learning.
The most intuitive examples are probably those figures that are commonly shown in tables after each match day.
They include points, goals scored, and goals conceded for all teams, which are then ranked depending on these numbers.

In the file "FeatureComputation.ipynb", many of these first features are computed and added to a data frame which
is finally saved in the file "data_with_features/1992-2021_mmb_3.csv".

This also includes features that are supposed to capture the recent shape of a team (by analysing only the data from
the most recent games), recent success at home and away and data about the last results when the two teams have met (if available).

A more detailed description of these first features can be found in the section "First Features" of the file "FeatureInfo.txt".
To see an explanation of all the abbreviations, see "Abbreviations.txt".



Further Feature Engineering
---------------------------

Further features, which can be computed from just the first features without going through the list of all previous games again,
are described in the section "Additional Features" in the file "FeatureInfo.txt". Their computation is implemented in the file "ModelDevelopment.ipynb" in the cell "Machine Learning with Additional Features". So far, they only comprise the following intuition:  
The bigger the current difference in points (or that at the end of the previous season), the bigger the likelihood for the home team to be
stronger than the away team. Both differences are exponentiated by a power of choice and coupled with a weight-coefficient which depends on the
match day.



Handling Games without Sufficient History
-----------------------------------------

Data rows of matches where there is no sufficient data from previous games of either of the two teams
will only be used to compute the 'First Features'.  
For simplicity, they are not used to train the models. Consequently, the resulting predictors will not work for games without sufficient history.  
The same holds for games where one of the teams did not participate in the same league in the previous season.



Data Leakage and Time-Dependent Test-Train Split
------------------------------------------------

Since the 'First Features' contain data from rows of previous games, data is leaking from games in the past to games in the future.  
A temporal train-test-split reduces train-test-contamination in a way that training data will not include information about test data.  
This is accomplished by fixing a date and then using all prior data for training- and all posterior data for testing-data.  
Unfortunately, this prevents the models from catching patterns that occur in the later seasons only.  
The features also make information leak from training- to test-data.



First Results
-------------

In a first attempt to predict whether a game will result in a home team win or not a home team win, the following accuracies were achieved:

| Model                                           | Binary Accuracy |
|-------------------------------------------------|-----------------|
| SciKit-Learn Logistic Regression   Classifier   |         62.96 % |
| SciKit-Learn Decision Tree         Classifier   |         63.54 % |
| SciKit-Learn Random Forest         Classifier   |         63.02 % |
| SciKit-Learn Support Vector        Classifier   |         63.44 % |
| SciKit-Learn K Nearest Neighbors   Classifier   |         60.48 % |
| SciKit-Learn Gaussian Naive Bayes  Classifier * |         63.49 % |
| XGBoost                                         |         63.60 % |
| Keras - TensorFlow Feedforward Neural Network   |         63.44 % |
| Keras - TensorFlow Decision Forest Classifier   |         62.22 % |

*) Although this classifier should be not used because of missing independence of the features, a certain hyperparameter setting
unexpectedly improved its performance which is an interesting effect.

MI scores of the features and first clustering- and PCA-results can be seen in the corresponding cells of the file "ModelDevelopment.ipynb".

Altogether, one should not expect a high accuracy of the predictors, since the amount of information we start with is quite limited
and a lot of things can happen in sports games that are hard to represent by numbers.

There is still a lot of room for improvement in feature engineering though, which should be investigated in the next steps.
